/*1. Конструкцію try...catch доречно використовувати, якщо у коді можуть бути помилки. Тобто для того, щоб код не впав при помильці, а ми мали змогу якось обробити цю помилку.
Найпоширініший приклад, це коли нам потрібно обробити данні с беку(наприклад), нам приходить формат JSON и ми далі ці данні парсимо і якщо у JSON будуть якісь помилки,
то наш скрипт впаде.
Також коли ми використовуємо prompt(наприклад) і очікуємо якоїсь інформації від користувача і використовуємо далі цю інформацію наприклад у якійсь функції, то при невірно введеній 
інформації також можуть бути помилки, які ми можемо відловити за допомогою try...catch. */

const books = [{
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

const root = document.getElementById('root');
const list = document.createElement('ul');
root.append(list);

function createItemsOfList(arr) {
    arr.forEach(elem => {
        try {
            if (elem.author && elem.name && elem.price) {
                const item = document.createElement('li');
                item.innerText = `Author: ${elem.author} , Name: ${elem.name} , Price: ${elem.price}`;
                list.append(item);
            }
            if (!elem.author) {
                throw new SyntaxError("Неповні дані: відсутнє поле author");
            }
            if (!elem.name) {
                throw new SyntaxError("Неповні дані: відсутнє поле name");
            }
            if (!elem.price) {
                throw new SyntaxError("Неповні дані: відсутнє поле price");
            }

        } catch (e) {
            console.log(e.message);
        }
    })
}

createItemsOfList(books);